<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// Admin Panel Routes (protected)
// Route::group([
//     'middleware' => 'auth', // Use auth middleware for admin panel
// ], function ($router) {

Route::group(['middleware' => 'Authenticate'], function () {
    Route::get('loginPage', [AuthController::class, 'loginPage'])->name('loginPage');
    Route::post('login', [AuthController::class, 'login'])->name('login');
    Route::post('storeData', [AuthController::class, 'storeData'])->name('storeData');
    Route::get('register', [AuthController::class, 'register'])->name('register');
    Route::get('allAdmin', [AuthController::class, 'allAdmin'])->name('allAdmin');
    Route::get('dashboard', [AuthController::class, 'dashboard'])->name('dashboard');
});
