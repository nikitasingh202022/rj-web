<?php

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\WebsiteController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Website Routes (unprotected)
Route::get('/', function () {
    return view('home');
})->name('home');
// Website Routes (unprotected)

Route::get('thanku', [WebsiteController::class, 'thanku'])->name('thanku');
Route::get('about', [WebsiteController::class, 'about'])->name('about');
Route::get('contact', [WebsiteController::class, 'contact'])->name('contact');
Route::get('media', [WebsiteController::class, 'media'])->name('media');
Route::get('blog', [WebsiteController::class, 'blog'])->name('blog');
Route::post('sendEmail', [WebsiteController::class, 'sendEmail'])->name('sendEmail');
