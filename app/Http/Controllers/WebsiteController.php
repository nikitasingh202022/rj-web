<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\Part\HtmlPart;

class WebsiteController extends Controller
{
    public function index()
    {
        return view('home');
    }

    public function about()
    {
        return view('about');
    }

    public function media()
    {
        return view('media');
    }

    public function contact()
    {
        return view('contact');
    }
    public function blog()
    {
        return view('blog');
    }


    public function sendEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|email',
            'phone' => 'required|numeric|digits:10',
            'message' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'failed',
                'message' => $validator->errors()->first()
            ], 400);
        } else {
            $name = $request->input('name');
            $email = $request->input('email');
            $phone = $request->input('phone');
            $message = $request->input('message');
            $subject = "Enquiry from ";

            try {

                $from = $email;
                $emailBody = "
                Hi,
                
                You have received a new enquiry from .
                
                Name: $name
                Email: $email
                Phone Number: $phone
                Message: $message
                
                Thanks,
               Rj
            ";

                // Send the email using PHP's direct mail function (insecure)
                mail($to = "nikitas.dollop@gmail.com", $subject, $emailBody, "From: $from");

                return redirect()->route('/'); // Redirect to a thank you page with a success message
            } catch (\Exception $e) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'There was an error sending your message. Please try again later.'
                ], 500);
            }
        }
    }
}
