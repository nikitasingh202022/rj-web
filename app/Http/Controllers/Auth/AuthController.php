<?php

namespace App\Http\Controllers\Auth;

use Validator;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    public function dashboard()
    {
        return view('Admin.dashbord');
    }

    public function register()
    {
        return view('Admin.register');
    }


    public function allAdmin()
    {
        return view('Admin.allAdmin');
    }


    public function loginPage()
    {
        return view('Admin.login');
    }



    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:6',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        if (!$token = auth()->attempt($validator->validated())) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return $this->createNewToken($token);
    }

    public function storeData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fullName' => ['required', 'string', 'max:250'],
            'userName' => ['required', 'string', 'max:250'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:tbl_user,email'],
            'number' => ['required', 'numeric', 'digits:10', 'unique:tbl_user,number'],
            'password' => ['required', 'min:4', 'confirmed'],
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'failed',
                'message' => $validator->errors()->first(),
            ], 400);
        } else {
            $fullName = $request->input('fullName') ? $request->input('fullName') : '';
            $userName = $request->input('userName') ? $request->input('userName') : '';
            $email = $request->input('email') ? $request->input('email') : '';
            $number = $request->input('number') ? $request->input('number') : '';
            $password = $request->input('password') ? $request->input('password') : '';

            $hashedPassword = Hash::make($password);
            $allData = [
                'fullName' => $fullName,
                'userName' => $userName,
                'email' => $email,
                'number' => $number,
                'password' => $hashedPassword
            ];
            $user_id = $request->user_id ? $request->user_id : '';
            $where = array(
                'user_id' => $user_id,
            );
            if ($user_id) {
                $updateUsers = User::where('user_id', $user_id)->update($allData);
                if ($updateUsers === false) {
                    return response()->json([
                        'status' => 'failed',
                        'message' => 'User is not found!',
                    ], 404);
                }
                $updatedUser = User::find($user_id);
                $response = [
                    'status' => 'success',
                    'message' => 'User updated successfully.',
                    'data' => $updatedUser,
                ];
            } else {
                $insertUser = User::create($allData);
                if (!$insertUser) {
                    return response()->json([
                        'status' => 'failed',
                        'message' => 'User could not be inserted!',
                    ], 500);
                }
                $response = [
                    'status' => 'success',
                    'message' => 'User inserted successfully.',
                    'data' => $insertUser,
                ];
            }
        }
    }

    public function logout()
    {
        auth()->logout();
        return response()->json(['message' => 'User successfully signed out']);
    }
    public function refresh()
    {
        return $this->createNewToken(auth()->refresh());
    }
    public function userProfile()
    {
        return response()->json(auth()->user());
    }


    protected function createNewToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user()
        ]);
    }


    //     public function loginUser(Request $request)
    //     {
    //         $request->validate([
    //             'email' => 'required|string|email',
    //             'password' => 'required|string',
    //         ]);
    //         $credentials = $request->only('email', 'password');

    //         $token = Auth::attempt($credentials);
    //         if (!$token) {
    //             return response()->json([
    //                 'status' => 'error',
    //                 'message' => 'Unauthorized',
    //             ], 401);
    //         }

    //         $user = Auth::user();
    //         return response()->json([
    //             'status' => 'Login success',
    //             'user' => $user,
    //             'authorisation' => [
    //                 'token' => $token,
    //                 'type' => 'bearer',
    //             ]
    //         ]);
    //     }

    //     // public function loginUser(Request $request)
    //     // {
    //     //     $request->validate([
    //     //         'password' => 'required|string',
    //     //     ]);

    //     //     // Retrieve email or number from the request
    //     //     $loginField = $request->input('email') ?? $request->input('number');

    //     //     $credentials = [
    //     //         'password' => $request->input('password'),
    //     //     ];

    //     //     // if (filter_var($loginField, FILTER_VALIDATE_EMAIL)) {
    //     //     //     $credentials['email'] = $loginField;
    //     //     // } else {

    //     //     //     $credentials['number'] = $loginField;
    //     //     // }
    //     //     if ($loginField != '') {

    //     //         $user = User::select('name', 'email', 'password')
    //     //             ->where('email', $loginField['email'] ?? null)
    //     //             ->orWhere('number', $loginField['number'] ?? null)
    //     //             ->get();
    //     //         // print_r($user);
    //     //         // die;
    //     //         if ($user && Auth::attempt($credentials)) {
    //     //             return "Login successfully";
    //     //         } else {
    //     //             return "Invalid Credentials";
    //     //         }
    //     //     } else
    //     //         return "Not Found credentials";
    //     // }

    //     public function index(Request $request)
    //     {
    //         if ($request->ajax()) {
    //             $data = User::latest()->get();
    //             return Datatables::of($data)
    //                 ->addIndexColumn()
    //                 ->addColumn('action', function ($row) {

    //                     $btn = '<a href="javascript:void(0)" class="edit btn btn-primary btn-sm">View</a>';

    //                     return $btn;
    //                 })
    //                 ->rawColumns(['action'])
    //                 ->make(true);
    //         }

    //         return view('users');
    //     }
}
