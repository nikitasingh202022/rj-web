<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.Raghuraj Singh', 'Raghuraj Singh') }}</title>

    <!-- Fonts -->
    <!-- <link rel="dns-prefetch" href="//fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
   
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,300,400,500,600,700,800,900" rel="stylesheet"> -->

    <link rel="stylesheet" href="{{ asset('css/open-iconic-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/animate.css')}}">

    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css')}}">

    <link rel="stylesheet" href="{{ asset('css/aos.css')}}">

    <link rel="stylesheet" href="{{ asset('css/ionicons.min.css')}}">

    <link rel="stylesheet" href="{{ asset('css/flaticon.css')}}">
    <link rel="stylesheet" href="{{ asset('css/icomoon.css')}}">
    <link rel="stylesheet" href="{{ asset('css/style.css')}}">
    <!-- <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css')}}"> -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" rel="stylesheet">
    <style id='utech-main-style-inline-css'>
        .banner-area {
            text-align: center;
        }

        .page-title h1,
        .page-title,
        .breadcumb,
        .breadcumb a,
        .breadcumb a span {
            color: #ffffff;
        }

        .navbar-header h3 a {
            color: #ffffff;
        }

        .is-sticky .navbar-header h3 a {
            color: #00152e;
        }

        @media (max-width: 991px) and (min-width: 768px) {
            .navbar-header h3 a {
                color: #00152e;
            }

            .is-sticky .navbar-header h3 a {
                color: #0045ff;
            }
        }

        @media only screen and (max-width: 767px) {
            .navbar-header h3 a {
                color: #00152e;
            }

            .is-sticky .navbar-header h3 a {
                color: #0045ff;
            }
        }

        .banner-area-bg::after {
            opacity: 0.70;
        }

        #main-nav {
            margin-right: inherit;
        }

        .mainmenu-area-bg {
            opacity: .0;
        }

        .is-sticky .mainmenu-area-bg {
            opacity: .1;
        }

        ul#nav li a {
            color: #223c68;
        }

        .is-sticky ul#nav li a,
        ul#nav li li a {
            color: #223c68;
        }

        ul#nav li a:hover,
        ul#nav li>a.active,
        ul#nav li.current-menu-parent>a,
        ul#nav li.current-menu-item>a,
        ul#nav li.hover>a,
        ul#nav li:hover>a {
            color: #2caad9;
        }

        .is-sticky ul#nav li>a:hover,
        .is-sticky ul#nav>li:hover>a,
        .is-sticky ul#nav li ul li>a:hover,
        .is-sticky ul#nav li>a.active,
        .is-sticky ul#nav li.hover>a,
        .is-sticky ul#nav li.current-menu-parent>a,
        .is-sticky ul#nav li.current-menu-item>a {
            color: #2caad9;
        }

        ul#nav li li a,
        .is-sticky ul#nav li li a {
            color: #ffffff !important;
        }

        ul#nav li ul li.hover>a,
        ul#nav li.has-sub li.current-menu-item>a,
        .is-sticky ul#nav li ul li.hover>a {
            color: #2caad9 !important;
            background: transparent !important;
        }

        .mainmenu-area {
            border-color: rgba(255, 255, 255, .15);
        }

        @media only screen and (max-width: 991px) {
            .mainmenu-area {
                border-color: rgba(255, 255, 255, .15);
            }

            .mainmenu-area-bg {
                opacity: .1;
            }

            .is-sticky .mainmenu-area-bg {
                opacity: .1;
            }

            .menu-toggle.full {
                color: #0a0a0a !important;
                border-color: #0a0a0a;
            }

            .line {
                stroke: #0a0a0a;
            }

            .is-sticky .menu-toggle.full {
                color: #202030;
                border-color: #202030;
            }

            .is-sticky .line {
                stroke: #202030;
            }

            ul#nav li a,
            ul#nav li li a,
            .is-sticky ul#nav li a,
            .is-sticky ul#nav li li a {
                color: #00274e !important;
            }

            ul#nav li.has-sub.open>a,
            ul#nav>li>a:hover,
            ul#nav li>a.active,
            ul#nav li.current-menu-item>a,
            ul#nav li.has-sub li.current-menu-item>a,
            ul#nav li.open.menu-item-has-children>a {
                background: #ffffff !important;
                color: #0045ff !important;
            }
        }

        .footer-area-bg:after {
            opacity: 0.5;
        }
    </style>

    <!-- Footer -->
    <style media="screen">
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }

        body {
            background: #fcfcfc;
            font-family: sans-serif;
        }

        footer {
            background: #0c3055;
            color: #fff;
            /* padding-top: 40px; */
            /* padding-bottom: 40px; */
            padding-bottom: -22px;
        }

        .footer-content {
            text-align: center;
        }

        .footer-content h3 {
            font-size: 2.1rem;
            font-weight: 500;
            text-transform: capitalize;
            line-height: 3rem;
        }

        .footer-content p {
            max-width: 500px;
            margin: 10px auto;
            line-height: 28px;
            font-size: 14px;
            color: #cacdd2;
        }

        .socials {
            list-style: none;
            display: flex;
            align-items: center;
            justify-content: center;
            margin-top: 20px;
        }

        .socials li {
            margin: 0 10px;
        }

        .socials a {
            text-decoration: none;
            color: #fff;
            border: 1px solid white;
            padding: 5px;
            border-radius: 50%;
        }

        .socials a i {
            font-size: 1.1rem;
            transition: color .4s ease;
        }

        .socials a:hover i {
            color: aqua;
        }

        .footer-bottom {
            background: #000;
            padding: 20px;
            text-align: center;
        }

        .footer-bottom p {
            float: left;
            font-size: 14px;
            word-spacing: 2px;
            text-transform: capitalize;
        }

        .footer-bottom p a {
            color: #44bae8;
            font-size: 16px;
            text-decoration: none;
        }

        .footer-bottom span {
            text-transform: uppercase;
            opacity: .4;
            font-weight: 200;
        }

        .footer-menu {
            float: right;
        }

        .footer-menu ul {
            display: flex;
        }

        .footer-menu ul li {
            padding-right: 10px;
            display: block;
        }

        .footer-menu ul li a {
            color: #cfd2d6;
            text-decoration: none;
        }

        .footer-menu ul li a:hover {
            color: #27bcda;
        }

        @media (max-width:500px) {
            .footer-menu ul {
                display: flex;
                margin-top: 10px;
                margin-bottom: 20px;
            }
        }
    </style>
    <style>
        .about-mf .box-shadow-full {
            padding-top: 4rem;
            padding-bottom: 4rem;
        }

        .about-mf .about-img {
            margin-bottom: 2rem;
        }

        .about-mf .about-img img {
            margin-left: 10px;
        }

        .skill-mf .progress {
            margin: .5rem 0 1.2rem 0;
            border-radius: 0;
            height: .7rem;
        }

        .skill-mf .progress .progress-bar {
            height: .7rem;
            background-color: #ffbd39;
        }

        /* Animation styles */
        #typing-animation {
            position: relative;
            font-size: 30px;
            font-weight: bold;
            color: rgb(255, 255, 255);
            overflow: hidden;
            white-space: nowrap;
            animation: typing 3s steps(20, end) infinite;
        }

        #typing-animation:before {
            content: "";
            top: 0;
            left: 0;
            width: 0;
            height: 100%;
            background-color: #ccc;
            animation: typing-cursor 0.5s ease-in-out infinite;
        }

        @keyframes typing {
            from {
                width: 0;
            }

            to {
                width: 100%;
            }
        }

        @keyframes typing-cursor {
            from {
                width: 5px;
            }

            to {
                width: 0;
            }
        }

        /* project image zoom effect */
        .zoom-effect {
            overflow: hidden;
            transition: transform 0.3s ease-out;
        }

        .zoom-effect:hover {
            transform: scale(1.1);
        }
    </style>

</head>

<body>
    <div id="app">
        <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar ftco-navbar-light site-navbar-target sleep" id="ftco-navbar">
            <div class="container">
                <a class="navbar-brand" href="{{URL::to('/')}}.html">Raghuraj Singh</a>
                <button class="navbar-toggler js-fh5co-nav-toggle fh5co-nav-toggle" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="oi oi-menu"></span> Menu
                </button>

                <div class="collapse navbar-collapse" id="ftco-nav">
                    <ul class="navbar-nav nav ml-auto">
                        <li class="nav-item"><a href="{{URL::to('/')}}" class="nav-link"><span>Home</span></a></li>
                        <li class="nav-item"><a href="{{URL::to('about')}}" class="nav-link"><span>About</span></a></li>
                        <li class="nav-item"><a href="{{URL::to('/media')}}" class="nav-link"><span>Media</span></a></li>
                        <li class="nav-item active"><a href="{{URL::to('/contact')}}" class="nav-link"><span>Contact</span></a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <main>
            <!-- class="py-4" -->
            @yield('content')
        </main>
        <footer>
            <section class="contact-section py-5">
                <div class="footer-content">
                    <h3>Foolish Developer</h3>
                    <p>Raj Template is a blog website where you will find great tutorials on web design and development. Here each tutorial is beautifully described step by step with the required source code.</p>
                    <ul class="socials">
                        <li><a href="https://www.facebook.com/ImRaghurajSingh/"><i class="fab fa-facebook"></i></a></li>
                        <li><a href="https://www.linkedin.com/in/imraghurajsingh?utm_source=share&utm_campaign=share_via&utm_content=profile&utm_medium=android_app"><i class="fab fa-linkedin"></i></a></li>
                        <li><a href="https://twitter.com/imraghurajsingh"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="https://www.instagram.com/imraghurajsingh?utm_source=ig_web_button_share_sheet&igsh=ZDNlZDc0MzIxNw=="><i class="fab fa-instagram"></i></a></li>
                    </ul>
                </div>
            </section>
        </footer>


        <!-- loader -->
        <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
                <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
                <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00" />
            </svg></div>
        <script src="{{asset('js/jquery.min.js')}}"></script>
        <script src="{{asset('js/jquery-migrate-3.0.1.min.js')}}"></script>
        <script src="{{asset('js/popper.min.js')}}"></script>
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
        <script src="{{asset('js/jquery.easing.1.3.js')}}"></script>
        <script src="{{asset('js/jquery.waypoints.min.js')}}"></script>
        <script src="{{asset('js/jquery.stellar.min.js')}}"></script>
        <script src="{{asset('js/owl.carousel.min.js')}}"></script>
        <script src="{{asset('js/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{asset('js/aos.js')}}"></script>
        <script src="{{asset('js/jquery.animateNumber.min.js')}}"></script>
        <script src="{{asset('js/scrollax.min.js')}}"></script>

        <script src="{{asset('js/main.js')}}"></script>
</body>

</html>