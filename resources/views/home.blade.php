@extends('layouts.app')

@section('content')
<style>
    #my-element {
        background-image: url(UserImage/bg_1.png);
        height: 981px !important;
        background-repeat: repeat-y;
        width: 34rem;
    }
</style>
<section id="home-section" class="hero">
    <div class="home-slider owl-carousel">
        <div class="slider-item">
            <!-- <div class="overlay"></div> -->
            <div class="container">
                <div class="row d-md-flex no-gutters slider-text align-items-end justify-content-end" data-scrollax-parent="true">
                    <div class="one-third js-fullheight order-md-last img" id="my-element">
                        <!-- <div class="overlay"></div> -->
                    </div>
                    <div class="one-forth d-flex align-items-center ftco-animate" data-scrollax="properties: { translateY: '70%' }">
                        <div class="text">
                            <span class="subheading">Hello!</span>
                            <h1 class="mb-4 mt-3">I'm <span>Raghuraj Singh</span></h1>

                            <!-- Element to contain animated typing -->
                            <span id="typing-animation"></span>

                            <script>
                                const typingAnimationElement = document.getElementById('typing-animation');

                                const typingTexts = [
                                    'Ceo  ',
                                    'Entrepreneur  ',
                                    'Founder   ',
                                ];

                                function playTypingAnimation(text) {
                                    for (let i = 0; i < text.length; i++) {
                                        setTimeout(() => {
                                            typingAnimationElement.textContent += text[i];
                                        }, i * 200);
                                    }

                                    setTimeout(() => {
                                        typingAnimationElement.textContent = '';
                                        playTypingAnimation(typingTexts[(typingTexts.indexOf(text) + 1) % typingTexts.length]);
                                    }, text.length * 200);
                                }

                                playTypingAnimation(typingTexts[0]);
                            </script>

                            <br>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection